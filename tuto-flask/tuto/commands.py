from .app import manager, db


@manager.command
def syncdb():
	db.create_all()

@manager.command
def loaddb(filename):
	db.create_all()

	import yaml
	albums = yaml.load(open(filename))

	from .models import Albums

	for b in albums:
		o = Albums(id = b["entryId"],
			by= b["by"],
			img=b["img"],
			title =b["title"])
		db.session.add(o)
	db.session.commit()

def newuser(username, password):
	from .models import User
	from hashlib import sha256
	m = sha256()
	m.update(password.encode())
	u = User(username = username, password = m.hexdigest())
	db.session.add(u)
	db.session.commit()