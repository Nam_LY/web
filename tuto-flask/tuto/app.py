from flask import Flask
app = Flask(__name__)
app.debug = True
app.config['BOOTSTRAP_SERVE_LOCAL'] = True
app.config['SECRET_KEY'] ="4263527d-f07f-4748-b78f-61e081ff27ad"

from flask.ext.bootstrap import Bootstrap
Bootstrap(app)


import os.path
def mkpath(p):
	return os.path.normpath(os.path.join(os.path.dirname(__file__),p))

from flask.ext.sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI'] =('sqlite:///'+mkpath('../albums.db'))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)


from flask.ext.script import Manager
manager = Manager(app)


from flask.ext.login import LoginManager
login_manager = LoginManager(app)

