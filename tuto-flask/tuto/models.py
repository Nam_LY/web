import yaml, os.path
from .app import db
from flask.ext.login import UserMixin
from .app import login_manager


def get_sample(debut=0 ,nb =100):
	return Albums.query.offset(debut).limit(nb).all()

class Albums(db.Model):
	id = db.Column(db.Integer,primary_key = True)
	by = db.Column(db.String(120))
	img = db.Column(db.String(90))
	title = db.Column(db.String(120))

	
	def get_author(id):
	    return Albums.query.get(id)

class User(db.Model, UserMixin):
	username = db.Column(db.String(50), primary_key = True)
	password = db.Column(db.String(64))

	def get_id(self):
		return self.username



@login_manager.user_loader
def load_user(username):
	return User.query.get(username)