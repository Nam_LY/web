from .app import app
from flask import render_template
from .models import get_sample
from wtforms.validators import DataRequired
from wtforms import PasswordField ,StringField, HiddenField
from . models import User
from hashlib import sha256
from flask.ext.login import login_user, current_user,logout_user
from flask import request
from flask import url_for , redirect
from .app import db
from . models import Albums
from flask.ext.wtf import Form


@app.route("/")
def home():
	return render_template("home.html", title="Mediatheque", 
		albums = get_sample())

class AlbumsForm(Form):
	id = HiddenField('id')
	name = StringField('Nom', validators=[DataRequired()])



@app.route("/save/author/", methods=("POST",))
def save_author():
	a = None
	f = AuthorFrom()
	if f.validate_on_submit():
		id = int(f.id.data)
		a = get_author(id)
		a.name = f.name.data
		db.session.commit()
		return redirect( url_for('one_author', id = a.id))
	a = get_author(int(f.id.data))
	return render_template("edit-author.html", author = a , form = f)


@app.route("/edit/author/<int:id>")
def edit_author(id):
	a = get_author(id)
	f = AlbumsForm(id = a.id , name = a.name)
	return render_template("edit-author.html", author = a, form = f)


class LoginForms(Form):
	username = StringField('Username')
	password = PasswordField('Password')

	def get_authenticated_user(self):
		user = User.query.get(self.username.data)
		if user is None:
			return None
		m = sha256()
		m.update(self.password.data.encode())
		passwd = m.hexdigest()
		return user if passwd == user.password else None



@app.route("/login/", methods=("GET","POST",))
def login():
	f = LoginForm()
	if f.validate_on_submit():
		user = f.get_authenticated_user()
		if user:
			login_user(user)
			return redirect(url_for("home"))
	return render_template("login.html", form = f)



@app.route("/logout/")
def logout():
	logout_user()
	return redirect( url_for('home'))







